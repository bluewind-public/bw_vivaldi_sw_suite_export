/**
 ******************************************************************************
 * @file    AudioLoop/Src/main.c
 * @author  Central Labs
 * @version V1.1.0
 * @date    27-Apr-2017
 * @brief   Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <string.h> /* strlen */
#include <stdio.h>  /* sprintf */
#include <math.h>   /* trunc */

#include "main.h"
#include "stm32l4xx_hal_tim.h"

#include "network.h"
#include "network_data.h"

#include "wave.h"

#include "ble_main.h"
#include "TargetFeatures.h"
#include "sensor_service.h"
#include "bluenrg_utils.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define AUDIO_CHANNELS 				        1
#define AUDIO_SAMPLING_FREQUENCY 		        48000
#define AUDIO_IN_BUF_LEN   (AUDIO_CHANNELS*AUDIO_SAMPLING_FREQUENCY/1000)
//#define AUDIO_OUT_BUF_LEN  ( 13073 )     /* (AUDIO_IN_BUF_LEN*8) 1s */
#define AUDIO_OUT_BUF_LEN  ( 7560 )     /* (AUDIO_IN_BUF_LEN*8) 0.5s */

#define TIM2_CLK_ENABLE                  __HAL_RCC_TIM2_CLK_ENABLE
#define TIM2_FORCE_RESET()               __HAL_RCC_USART2_FORCE_RESET()
#define TIM2_RELEASE_RESET()             __HAL_RCC_USART2_RELEASE_RESET()

/* Private macro -------------------------------------------------------------*/

#define CLASS_NONE  0
#define CLASS_GOING  1
#define CLASS_COMING  2

/* Private variables ---------------------------------------------------------*/
static volatile uint32_t HCI_ProcessEvent=0;
static volatile uint32_t SendEnv         =0;

uint16_t PCM_Buffer[AUDIO_IN_BUF_LEN];
static int16_t audio_out_buffer[AUDIO_OUT_BUF_LEN];
int8_t PCM_StoredIdx[12] = {0, 4, 9, 13, 17, 22, 26, 30, 35, 39, 43, 47};

static void *PCM1774_X_0_handle = NULL;

//static float mfcc[AI_NETWORK_IN_HEIGHT][AI_NETWORK_IN_WIDTH][AI_NETWORK_IN_CHANNELS] __attribute__ ((section (".ram2")));
//static float mfcc[20][22][1] __attribute__ ((section (".ram2"))); // 1s
static float mfcc[20][11][1] __attribute__ ((section (".ram2")));
static float mfcc_export[220] __attribute__ ((section (".ram2")));

/* Global handle to reference the instantiated NN */
static ai_handle network = AI_HANDLE_NULL;

/* Global buffer to store the activation data - R/W data */
static ai_u8 activations[AI_NETWORK_DATA_ACTIVATIONS_SIZE];

/* Prescaler value declartion*/
uint32_t uwPrescalerValue = 0;

TIM_HandleTypeDef    TimHandleAudio;

extern TIM_HandleTypeDef TimHandle;
extern void CDC_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

extern uint8_t set_connectable;
extern int connected;

USBD_HandleTypeDef  USBD_Device;

uint32_t ConnectionBleStatus  =0;

uint8_t BufferToWrite[256];
int32_t BytesToWrite;

TIM_HandleTypeDef TimCCHandle;

uint8_t bdaddr[6];

/* Timer One Pulse Configuration Structure declaration */
TIM_OnePulse_InitTypeDef sConfig;

typedef enum status
{
	none = 0,
	going,
	coming
}car_status;

ai_float going_values[100];
ai_float none_values[100];
ai_float coming_values[100];
ai_int   values_idx = 0;

car_status old_status = none;
car_status actual_status = none;
uint8_t car_detected = 0;

uint8_t buffer_status[2] = { 0, 0};

/* Private function prototypes -----------------------------------------------*/
static uint8_t BlinkSignal(void);
static void Init_BlueNRG_Custom_Services(void);
static void Init_BlueNRG_Stack(void);
static void InitTimers(void);
static void SendEnvironmentalData(uint8_t *ble_data);

/* Private functions ---------------------------------------------------------*/

int network_init( void )
{
	__HAL_RCC_CRC_CLK_ENABLE();
	ai_network_create(&network, AI_NETWORK_DATA_CONFIG);
    if (!network)
    	return -1;

    const ai_network_params params = {
    AI_NETWORK_DATA_WEIGHTS(ai_network_data_weights_get()),
    AI_NETWORK_DATA_ACTIVATIONS(activations) };

  if (!ai_network_init(network, &params))
	return -2;

  return 0;
}

int classify(const ai_float *in_data, ai_float *out_data)
{
   /* initialize input/output buffer handlers */

  static ai_buffer ai_input[AI_NETWORK_IN_NUM] = { AI_NETWORK_IN_1 };
  static ai_buffer ai_output[AI_NETWORK_OUT_NUM] = { AI_NETWORK_OUT_1 };

  ai_i32 nbatch;
  ai_u16 batch_size = 1;

  ai_input[0].n_batches = batch_size;
  ai_input[0].data = AI_HANDLE_PTR(in_data);
  ai_output[0].n_batches = batch_size;
  ai_output[0].data = AI_HANDLE_PTR(out_data);

  __HAL_RCC_CRC_CLK_ENABLE();
  nbatch = ai_network_run(network, &ai_input[0], &ai_output[0]);
  if (nbatch != batch_size)
    return -3;

  return 0;
}

/**
  * @brief  TIM_Config: Configure TIMx timer
  * @param  None.
  * @retval None.
  */
static void TIM_Config(void)
{
  /* Set TIMx instance */
  TimHandleAudio.Instance = TIM2;

  /* Initialize TIM2 peripheral as follow:
       + Period = 10000 - 1
       + Prescaler = ((SystemCoreClock/2)/10000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandleAudio.Init.Period            = 1000 - 1;
  TimHandleAudio.Init.Prescaler         = uwPrescalerValue;
  TimHandleAudio.Init.ClockDivision     = 0;
  TimHandleAudio.Init.CounterMode       = TIM_COUNTERMODE_DOWN;
  TimHandleAudio.Init.RepetitionCounter = 0;
  if(HAL_TIM_Base_Init(&TimHandleAudio) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
* @brief  Function for initializing timers for sending the information to BLE:
 *  - 1 for sending MotionFX/AR/CP and Acc/Gyro/Mag  (not used in Vivaldi)
 *  - 1 for sending the Environmental info
 * @param  None
 * @retval None
 */
static void InitTimers(void)
{
  uint32_t uwPrescalerValue;

  /* Timer Output Compare Configuration Structure declaration */
  TIM_OC_InitTypeDef sConfig;

  /* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
  uwPrescalerValue = (uint32_t) ((SystemCoreClock / 10000) - 1);

  /* Set TIM1 instance */
  TimCCHandle.Instance = TIM1;
  TimCCHandle.Init.Period        = 65535;
  TimCCHandle.Init.Prescaler     = uwPrescalerValue;
  TimCCHandle.Init.ClockDivision = 0;
  TimCCHandle.Init.CounterMode   = TIM_COUNTERMODE_UP;
  if(HAL_TIM_OC_Init(&TimCCHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

 /* Configure the Output Compare channels */
 /* Common configuration for all channels */
  sConfig.OCMode     = TIM_OCMODE_TOGGLE;
  sConfig.OCPolarity = TIM_OCPOLARITY_LOW;

  /* Output Compare Toggle Mode configuration: Channel1 */
  sConfig.Pulse = uhCCR1_Val;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Configuration Error */
    Error_Handler();
  }
}


static int8_t AudioLED_Itf_Init(void)
{
  /*##-2- Enable TIM peripherals Clock #######################################*/
  TIM2_CLK_ENABLE();

   /*##-3- Configure the NVIC for TIMx ########################################*/
  /* Set Interrupt Group Priority */
  HAL_NVIC_SetPriority(TIM2_IRQn, 0x6, 0);

  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM2_IRQn);

  /*##-3- Configure the TIM Base generation  #################################*/
  TIM_Config();

  /*##-4- Start the TIM Base generation in interrupt mode ####################*/
  /* Start Channel1 */
  if(HAL_TIM_Base_Start_IT(&TimHandleAudio) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }

  return (0);
}

static uint8_t BlinkSignal(void)
{
	/* Turn on LED1 and Start the TIM Base generation in interrupt mode */
	BSP_LED_On(LED1);

	if (HAL_TIM_Base_Start_IT(&TimHandleAudio) != HAL_OK)
	{
	/* Initialization Error */
	Error_Handler();
	}

	return 0;
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main( void ) 
{
  uint32_t StartTime;

  /* STM32F4xx HAL library initialization:
  - Configure the Flash prefetch, instruction and Data caches
  - Configure the Systick to generate an interrupt each 1 msec
  - Set NVIC Group Priority to 4
  - Global MSP (MCU Support Package) initialization
  */
  HAL_Init();
  
  /* Configure the system clock */
  SystemClock_Config();
  
  /* Configure and disable all the Chip Select pins */
  Sensor_IO_SPI_CS_Init_All();

  InitTargetPlatform(TARGET_SENSORTILE);

  /* Configure Audio Output peripheral (SAI) and external DAC */
  BSP_AUDIO_OUT_Init(PCM1774_0, &PCM1774_X_0_handle, 0, 20, AUDIO_SAMPLING_FREQUENCY);
  BSP_AUDIO_OUT_SetVolume(PCM1774_X_0_handle, 20);

  /* Configure Audio Input peripheral - DFSDM */
  BSP_AUDIO_IN_Init(AUDIO_SAMPLING_FREQUENCY, 16, AUDIO_CHANNELS);

  BSP_LED_Init(LED1);

  BlinkSignal();

  /* Compute the prescaler value to have TIMx counter clock equal to 10000 Hz */
  uwPrescalerValue = ((uint32_t)(SystemCoreClock / 10000) - 1);

  /* enable USB power on Pwrctrl CR2 register */
  HAL_PWREx_EnableVddUSB();
  /*** USB CDC Configuration ***/
  /* Init Device Library */
  USBD_Init(&USBD_Device, &VCP_Desc, 0);
  /* Add Supported Class */
  USBD_RegisterClass(&USBD_Device, USBD_CDC_CLASS);
  /* Add Interface callbacks for AUDIO and CDC Class */
  USBD_CDC_RegisterInterface(&USBD_Device, &USBD_CDC_fops);
  /* Start Device Process */
  USBD_Start(&USBD_Device);

  /* Initialize the BlueNRG */
  Init_BlueNRG_Stack();

  /* Initialize the BlueNRG Custom services */
  Init_BlueNRG_Custom_Services();

  AudioLED_Itf_Init();

  /* initialize timers */
  InitTimers();

  StartTime = HAL_GetTick();

  /* Start Microphone acquisition */
  BSP_AUDIO_IN_Record(PCM_Buffer,0);
  
  while (1)
  {
	/* Led Blinking when there is not a client connected */
	if(!connected) {
	  if(!TargetBoardFeatures.LedStatus) {
		if(HAL_GetTick()-StartTime > 1000) {
		  LedOnTargetPlatform();
		  TargetBoardFeatures.LedStatus =1;
		  StartTime = HAL_GetTick();
		}
	  } else {
		if(HAL_GetTick()-StartTime > 50) {
		  LedOffTargetPlatform();
		  TargetBoardFeatures.LedStatus =0;
		  StartTime = HAL_GetTick();
		}
	  }
	}

	/* handle BLE event */
	if(HCI_ProcessEvent) {
	  HCI_ProcessEvent=0;
	  HCI_Process();
	}

	/* Update the BLE advertise data and make the Board connectable */
	if(set_connectable){
	  setConnectable();
	  set_connectable = FALSE;
	}

	/* Environmental Data */
	if(SendEnv) {
	  SendEnv=0;
	  //SendEnvironmentalData();
	  buffer_status[0] = (uint8_t)actual_status;
	  buffer_status[1] = car_detected;
	  SendEnvironmentalData(buffer_status);
	}

    /* Go to Sleep, everything is done in the interrupt service routines */
    __WFI();
  }
}

/**
* @brief  User function that is called when 1 ms of PDM data is available.
* 		  In this application only PDM to PCM conversion and USB streaming
*                  is performed.
* 		  User can add his own code here to perform some DSP or audio analysis.
* @param  none
* @retval None
*/
void AudioProcess(void)
{
  /*for L4 PDM to PCM conversion is performed in hardware by DFSDM peripheral*/
  static uint32_t IndexOut = 1024;
  uint32_t indexIn;
  char data_s[64];
  int size;

  /* we store one every four sample in order to get a 12kHz frequency */
  for(indexIn=0;indexIn< ( AUDIO_IN_BUF_LEN / 4 ) ;indexIn++)
  {
    audio_out_buffer[IndexOut++] = PCM_Buffer[PCM_StoredIdx[indexIn]];
  }

  if(IndexOut >= ( AUDIO_OUT_BUF_LEN - 1024 ) )
  {
      BSP_AUDIO_IN_Stop();

      AudioPreProcess_mfcc(audio_out_buffer, mfcc);

      ai_float out_data[AI_NETWORK_OUT_1_SIZE];

      network_init();

      classify((ai_float *)mfcc, out_data);

      for (int k = 0 ; k < 11 ; k++ )
      {
    	  for (int l = 0 ; l < 20 ; l++ )
    	  {
    		  mfcc_export[k*20+l] = mfcc[l][k][0];
    	  }
      }

      /* update actual status */
      if( out_data[CLASS_COMING] > out_data[CLASS_GOING] )
      {
    	  if ( out_data[CLASS_COMING] > out_data[CLASS_NONE] )
    	  {
        	  actual_status = coming;
        	  size = sprintf(data_s, "VIVALDI/DETECT/STATUS Approaching\n");
      	  }
    	  else
    	  {
        	  actual_status = none;
        	  size = sprintf(data_s, "VIVALDI/DETECT/STATUS None\n");
    	  }
      }
      else
      {
    	  if (out_data[CLASS_GOING] > out_data[CLASS_NONE] )
    	  {
			  actual_status = going;
			  size = sprintf(data_s, "VIVALDI/DETECT/STATUS Leaving\n");
    	  }
          else
          {
        	  actual_status = none;
        	  size = sprintf(data_s, "VIVALDI/DETECT/STATUS None\n");
          }
      }

      CDC_Fill_Buffer(( uint8_t * )data_s, strlen(data_s));

      /* update old status */
      if ( actual_status == going && old_status == coming )
      {
    	  BlinkSignal();
		  size = sprintf(data_s, "VIVALDI/DETECT/COUNTER %d\n", 1);
		  CDC_Fill_Buffer(( uint8_t * )data_s, strlen(data_s));
		  car_detected = 1;
    	  old_status = none;
      }
      else
      {
		  size = sprintf(data_s, "VIVALDI/DETECT/COUNTER %d\n", 0);
		  CDC_Fill_Buffer(( uint8_t * )data_s, strlen(data_s));
    	  old_status = actual_status;
    	  car_detected  = 0;
      }

      SendEnv=1;

      BSP_AUDIO_IN_Record(PCM_Buffer,0);

      IndexOut=0;
  }
}

/**
  * @brief  Send Environmetal Sound Data (Temperature/Pressure/Humidity) to BLE
  * @param  None
  * @retval None
  */
static void SendEnvironmentalData(uint8_t *ble_data)
{
  Environmental_Update(0,0,0,0, ble_data);
}

/** @brief Initialize the BlueNRG Stack
 * @param None
 * @retval None
 */
static void Init_BlueNRG_Stack(void)
{
  const char BoardName[8] = {NAME_STLBLE,0};
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  int ret;
  uint8_t  hwVersion;
  uint16_t fwVersion;

#ifdef STATIC_BLE_MAC
  {
    uint8_t tmp_bdaddr[6]= {STATIC_BLE_MAC};
    int32_t i;
    for(i=0;i<6;i++)
      bdaddr[i] = tmp_bdaddr[i];
  }
#endif /* STATIC_BLE_MAC */

  /* Initialize the BlueNRG SPI driver */
  BNRG_SPI_Init();

  /* Initialize the BlueNRG HCI */
  HCI_Init();

  /* Reset BlueNRG hardware */
  BlueNRG_RST();

  /* get the BlueNRG HW and FW versions */
  getBlueNRGVersion(&hwVersion, &fwVersion);

  if (hwVersion > 0x30) {
    /* X-NUCLEO-IDB05A1 expansion board is used */
    TargetBoardFeatures.bnrg_expansion_board = IDB05A1;
  } else {
    /* X-NUCLEO-IDB0041 expansion board is used */
    TargetBoardFeatures.bnrg_expansion_board = IDB04A1;
  }

  /*
   * Reset BlueNRG again otherwise it will fail.
   */
  BlueNRG_RST();

#ifndef STATIC_BLE_MAC
  /* Create a Unique BLE MAC */
  {
    bdaddr[0] = (STM32_UUID[1]>>24)&0xFF;
    bdaddr[1] = (STM32_UUID[0]    )&0xFF;
    bdaddr[2] = (STM32_UUID[2] >>8)&0xFF;
    bdaddr[3] = (STM32_UUID[0]>>16)&0xFF;
    bdaddr[4] = (((STLBLE_VERSION_MAJOR-48)*10) + (STLBLE_VERSION_MINOR-48)+100)&0xFF;
    bdaddr[5] = 0xC0; /* for a Legal BLE Random MAC */
  }
#else /* STATIC_BLE_MAC */

  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET,
                                  CONFIG_DATA_PUBADDR_LEN,
                                  bdaddr);
  if(ret){
     STLBLE_PRINTF("\r\nSetting Pubblic BD_ADDR failed\r\n");
     goto fail;
  }
#endif /* STATIC_BLE_MAC */

  ret = aci_gatt_init();
  if(ret){
     STLBLE_PRINTF("\r\nGATT_Init failed\r\n");
     goto fail;
  }

  if (TargetBoardFeatures.bnrg_expansion_board == IDB05A1) {
    ret = aci_gap_init_IDB05A1(GAP_PERIPHERAL_ROLE_IDB05A1, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);
  } else {
    ret = aci_gap_init_IDB04A1(GAP_PERIPHERAL_ROLE_IDB04A1, &service_handle, &dev_name_char_handle, &appearance_char_handle);
  }

  if(ret != BLE_STATUS_SUCCESS){
     STLBLE_PRINTF("\r\nGAP_Init failed\r\n");
     goto fail;
  }

#ifndef  STATIC_BLE_MAC
  ret = hci_le_set_random_address(bdaddr);

  if(ret){
     STLBLE_PRINTF("\r\nSetting the Static Random BD_ADDR failed\r\n");
     goto fail;
  }
#endif /* STATIC_BLE_MAC */

  ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0,
                                   7/*strlen(BoardName)*/, (uint8_t *)BoardName);

  if(ret){
     STLBLE_PRINTF("\r\naci_gatt_update_char_value failed\r\n");
    while(1);
  }

  ret = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED,
                                     OOB_AUTH_DATA_ABSENT,
                                     NULL, 7, 16,
                                     USE_FIXED_PIN_FOR_PAIRING, 123456,
                                     BONDING);
  if (ret != BLE_STATUS_SUCCESS) {
     STLBLE_PRINTF("\r\nGAP setting Authentication failed\r\n");
     goto fail;
  }

  STLBLE_PRINTF("SERVER: BLE Stack Initialized \r\n"
         "\t\tBoard type=%s HWver=%d, FWver=%d.%d.%c\r\n"
         "\t\tBoardName= %s\r\n"
         "\t\tBoardMAC = %x:%x:%x:%x:%x:%x\r\n\n",
         "SensorTile",
         hwVersion,
         fwVersion>>8,
         (fwVersion>>4)&0xF,
         (hwVersion > 0x30) ? ('a'+(fwVersion&0xF)-1) : 'a',
         BoardName,
         bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]);

  /* Set output power level */
  aci_hal_set_tx_power_level(1,4);

  return;

fail:
  return;
}

/** @brief Initialize all the Custom BlueNRG services
* @param None
* @retval None
*/
static void Init_BlueNRG_Custom_Services(void)
{
  int ret;

  ret = Add_HWServW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("HW      Service W2ST added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding HW Service W2ST\r\n");
  }

  ret = Add_ConfigW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("Config  Service W2ST added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding Config Service W2ST\r\n");
  }
}

/**
  * @brief  Output Compare callback in non blocking mode
  * @param  htim : TIM OC handle
  * @retval None
  */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
  uint32_t uhCapture=0;

  /* TIM1_CH1 toggling with frequency = 2Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
  {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_1, (uhCapture + uhCCR1_Val));
    SendEnv=1;
  }
}

/**
* @brief  Transfer Complete user callback, called by BSP functions.
* @param  None
* @retval None
*/
void BSP_AUDIO_IN_TransferComplete_CallBack(void)
{
  AudioProcess();
}

/**
* @brief  Half Transfer Complete user callback, called by BSP functions.
* @param  None
* @retval None
*/
void BSP_AUDIO_IN_HalfTransfer_CallBack(void)
{
  AudioProcess();
}

/**
  * @brief This function provides accurate delay (in milliseconds) based
  *        on variable incremented.
  * @note This is a user implementation using WFI state
  * @param Delay: specifies the delay time length, in milliseconds.
  * @retval None
  */
void HAL_Delay(__IO uint32_t Delay)
{
  uint32_t tickstart = 0;
  tickstart = HAL_GetTick();
  while((HAL_GetTick() - tickstart) < Delay){
    __WFI();
  }
}

/**
 * @brief  This function is executed in case of error occurrence
 * @param  None
 * @retval None
 */
void Error_Handler( void )
{

  while (1)
  {}
}

/**
 * @brief  EXTI line detection callback.
 * @param  uint16_t GPIO_Pin Specifies the pins connected EXTI line
 * @retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch(GPIO_Pin){
    case BNRG_SPI_EXTI_PIN:
      HCI_Isr();
      HCI_ProcessEvent=1;
    break;
  }
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed( uint8_t *file, uint32_t line )
{

  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  
  while (1)
  {}
}

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
