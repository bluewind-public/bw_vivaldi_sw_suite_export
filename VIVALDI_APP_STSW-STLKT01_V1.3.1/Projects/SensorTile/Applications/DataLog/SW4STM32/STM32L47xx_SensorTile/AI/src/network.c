/**
  ******************************************************************************
  * @file    network.c
  * @author  AST Embedded Analytics Research Platform
  * @date    Mon Jan 21 11:03:28 2019
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2017 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */



#include "network.h"

#include "ai_platform_interface.h"
#include "ai_math_helpers.h"

#include "core_common.h"
#include "layers.h"

#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#define AI_TOOLS_VERSION_MAJOR 3
#define AI_TOOLS_VERSION_MINOR 2
#define AI_TOOLS_VERSION_MICRO 0

#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#define AI_TOOLS_API_VERSION_MAJOR 1
#define AI_TOOLS_API_VERSION_MINOR 0
#define AI_TOOLS_API_VERSION_MICRO 0

#undef AI_NET_OBJ_INSTANCE
#define AI_NET_OBJ_INSTANCE g_network
 
#undef AI_NETWORK_MODEL_SIGNATURE
#define AI_NETWORK_MODEL_SIGNATURE     "6973003be8bf67b1e943693b971885a0"

#ifndef AI_TOOLS_REVISION_ID
#define AI_TOOLS_REVISION_ID     "(rev-)"
#endif

#undef AI_TOOLS_DATE_TIME
#define AI_TOOLS_DATE_TIME   "Mon Jan 21 11:03:28 2019"

#undef AI_TOOLS_COMPILE_TIME
#define AI_TOOLS_COMPILE_TIME    __DATE__ " " __TIME__

#undef AI_NETWORK_N_BATCHES
#define AI_NETWORK_N_BATCHES         (1)

/* Forward network declaration */
AI_STATIC ai_network AI_NET_OBJ_INSTANCE;


/* Forward network arrays declaration */
AI_STATIC ai_array input_0_output_array;   /* Array #1 */
AI_STATIC ai_array conv2d_1_output_array;   /* Array #2 */
AI_STATIC ai_array batch_normalization_1_output_array;   /* Array #3 */
AI_STATIC ai_array max_pooling2d_1_output_array;   /* Array #4 */
AI_STATIC ai_array conv2d_2_output_array;   /* Array #5 */
AI_STATIC ai_array batch_normalization_2_output_array;   /* Array #6 */
AI_STATIC ai_array max_pooling2d_2_output_in_array;   /* Array #7 */
AI_STATIC ai_array max_pooling2d_2_output_out_array;   /* Array #8 */
AI_STATIC ai_array lstm_1_output_array;   /* Array #9 */
AI_STATIC ai_array batch_normalization_3_output_array;   /* Array #10 */
AI_STATIC ai_array activation_3_output_in_array;   /* Array #11 */
AI_STATIC ai_array activation_3_output_out_array;   /* Array #12 */
AI_STATIC ai_array dense_1_output_array;   /* Array #13 */
AI_STATIC ai_array activation_4_output_array;   /* Array #14 */
AI_STATIC ai_array dense_2_output_array;   /* Array #15 */
AI_STATIC ai_array activation_5_output_array;   /* Array #16 */
AI_STATIC ai_array dense_3_output_array;   /* Array #17 */
AI_STATIC ai_array activation_6_output_array;   /* Array #18 */


/* Forward network tensors declaration */
AI_STATIC ai_tensor input_0_output;   /* Tensor #1 */
AI_STATIC ai_tensor conv2d_1_output;   /* Tensor #2 */
AI_STATIC ai_tensor batch_normalization_1_output;   /* Tensor #3 */
AI_STATIC ai_tensor max_pooling2d_1_output;   /* Tensor #4 */
AI_STATIC ai_tensor conv2d_2_output;   /* Tensor #5 */
AI_STATIC ai_tensor batch_normalization_2_output;   /* Tensor #6 */
AI_STATIC ai_tensor max_pooling2d_2_output_in;   /* Tensor #7 */
AI_STATIC ai_tensor max_pooling2d_2_output_out;   /* Tensor #8 */
AI_STATIC ai_tensor lstm_1_output;   /* Tensor #9 */
AI_STATIC ai_tensor batch_normalization_3_output;   /* Tensor #10 */
AI_STATIC ai_tensor activation_3_output_in;   /* Tensor #11 */
AI_STATIC ai_tensor activation_3_output_out;   /* Tensor #12 */
AI_STATIC ai_tensor dense_1_output;   /* Tensor #13 */
AI_STATIC ai_tensor activation_4_output;   /* Tensor #14 */
AI_STATIC ai_tensor dense_2_output;   /* Tensor #15 */
AI_STATIC ai_tensor activation_5_output;   /* Tensor #16 */
AI_STATIC ai_tensor dense_3_output;   /* Tensor #17 */
AI_STATIC ai_tensor activation_6_output;   /* Tensor #18 */


/* Subgraph network operators declaration */



/* Forward network layers declaration */
AI_STATIC ai_layer_conv2d conv2d_1_layer; /* Layer #1 */
AI_STATIC ai_layer_bn batch_normalization_1_layer; /* Layer #2 */
AI_STATIC ai_layer_pool max_pooling2d_1_layer; /* Layer #3 */
AI_STATIC ai_layer_conv2d conv2d_2_layer; /* Layer #4 */
AI_STATIC ai_layer_bn batch_normalization_2_layer; /* Layer #5 */
AI_STATIC ai_layer_pool max_pooling2d_2_layer; /* Layer #6 */
AI_STATIC ai_layer_lstm lstm_1_layer; /* Layer #7 */
AI_STATIC ai_layer_bn batch_normalization_3_layer; /* Layer #8 */
AI_STATIC ai_layer_nl activation_3_layer; /* Layer #9 */
AI_STATIC ai_layer_dense dense_1_layer; /* Layer #10 */
AI_STATIC ai_layer_nl activation_4_layer; /* Layer #11 */
AI_STATIC ai_layer_dense dense_2_layer; /* Layer #12 */
AI_STATIC ai_layer_nl activation_5_layer; /* Layer #13 */
AI_STATIC ai_layer_dense dense_3_layer; /* Layer #14 */
AI_STATIC ai_layer_sm activation_6_layer; /* Layer #15 */


/* Arrays declarations section */
AI_ARRAY_OBJ_DECLARE(
  input_0_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 220,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 1440,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_1_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 1440,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  max_pooling2d_1_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 720,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  conv2d_2_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 300,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_2_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 300,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  max_pooling2d_2_output_in_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 60,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  max_pooling2d_2_output_out_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 60,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  lstm_1_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 100,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_3_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 100,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  activation_3_output_in_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 100,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  activation_3_output_out_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 100,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  dense_1_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 15,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  activation_4_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 15,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  dense_2_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 15,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  activation_5_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 15,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  dense_3_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 3,
  AI_STATIC )
AI_ARRAY_OBJ_DECLARE(
  activation_6_output_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 3,
  AI_STATIC )


/* Activations tensors declaration section */
AI_TENSOR_OBJ_DECLARE(
  input_0_output,
  AI_SHAPE_INIT(20, 11, 1, 1),
  AI_STRIDE_INIT(44, 4, 4, 4),
  &input_0_output_array,
  &input_0_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_output,
  AI_SHAPE_INIT(16, 9, 10, 1),
  AI_STRIDE_INIT(360, 40, 4, 4),
  &conv2d_1_output_array,
  &conv2d_1_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  batch_normalization_1_output,
  AI_SHAPE_INIT(16, 9, 10, 1),
  AI_STRIDE_INIT(360, 40, 4, 4),
  &batch_normalization_1_output_array,
  &batch_normalization_1_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  max_pooling2d_1_output,
  AI_SHAPE_INIT(8, 9, 10, 1),
  AI_STRIDE_INIT(360, 40, 4, 4),
  &max_pooling2d_1_output_array,
  &max_pooling2d_1_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_output,
  AI_SHAPE_INIT(5, 6, 10, 1),
  AI_STRIDE_INIT(240, 40, 4, 4),
  &conv2d_2_output_array,
  &conv2d_2_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  batch_normalization_2_output,
  AI_SHAPE_INIT(5, 6, 10, 1),
  AI_STRIDE_INIT(240, 40, 4, 4),
  &batch_normalization_2_output_array,
  &batch_normalization_2_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  max_pooling2d_2_output_in,
  AI_SHAPE_INIT(2, 3, 10, 1),
  AI_STRIDE_INIT(120, 40, 4, 4),
  &max_pooling2d_2_output_in_array,
  &max_pooling2d_2_output_in,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  max_pooling2d_2_output_out,
  AI_SHAPE_INIT(10, 1, 6, 1),
  AI_STRIDE_INIT(24, 24, 4, 4),
  &max_pooling2d_2_output_out_array,
  &max_pooling2d_2_output_out,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  lstm_1_output,
  AI_SHAPE_INIT(10, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &lstm_1_output_array,
  &lstm_1_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  batch_normalization_3_output,
  AI_SHAPE_INIT(10, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_3_output_array,
  &batch_normalization_3_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  activation_3_output_in,
  AI_SHAPE_INIT(10, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &activation_3_output_in_array,
  &activation_3_output_in,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  activation_3_output_out,
  AI_SHAPE_INIT(1, 1, 100, 1),
  AI_STRIDE_INIT(400, 400, 4, 4),
  &activation_3_output_out_array,
  &activation_3_output_out,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  dense_1_output,
  AI_SHAPE_INIT(1, 1, 15, 1),
  AI_STRIDE_INIT(60, 60, 4, 4),
  &dense_1_output_array,
  &dense_1_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  activation_4_output,
  AI_SHAPE_INIT(1, 1, 15, 1),
  AI_STRIDE_INIT(60, 60, 4, 4),
  &activation_4_output_array,
  &activation_4_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  dense_2_output,
  AI_SHAPE_INIT(1, 1, 15, 1),
  AI_STRIDE_INIT(60, 60, 4, 4),
  &dense_2_output_array,
  &dense_2_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  activation_5_output,
  AI_SHAPE_INIT(1, 1, 15, 1),
  AI_STRIDE_INIT(60, 60, 4, 4),
  &activation_5_output_array,
  &activation_5_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  dense_3_output,
  AI_SHAPE_INIT(1, 1, 3, 1),
  AI_STRIDE_INIT(12, 12, 4, 4),
  &dense_3_output_array,
  &dense_3_output,
  AI_STATIC)
AI_TENSOR_OBJ_DECLARE(
  activation_6_output,
  AI_SHAPE_INIT(1, 1, 3, 1),
  AI_STRIDE_INIT(12, 12, 4, 4),
  &activation_6_output_array,
  &activation_6_output,
  AI_STATIC)




/* Layer #1: "conv2d_1" (Conv2D) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_weights_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 150,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  conv2d_1_weights,
  AI_SHAPE_INIT(10, 5, 3, 1),
  AI_STRIDE_INIT(60, 12, 4, 4),
  &conv2d_1_weights_array,
  &conv2d_1_weights,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  conv2d_1_bias,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &conv2d_1_bias_array,
  &conv2d_1_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  conv2d_1_layer, 0,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &batch_normalization_1_layer, AI_STATIC ,
  .in = &input_0_output, .out = &conv2d_1_output, 
  .weights = &conv2d_1_weights, 
  .bias = &conv2d_1_bias, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_2D_INIT(0, 0), 
);
/* Layer #2: "batch_normalization_1" (ScaleBias) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_1_scale_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  batch_normalization_1_scale,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_1_scale_array,
  &batch_normalization_1_scale,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_1_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  batch_normalization_1_bias,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_1_bias_array,
  &batch_normalization_1_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  batch_normalization_1_layer, 2,
  BN_TYPE,
  bn, forward_bn,
  &AI_NET_OBJ_INSTANCE, &max_pooling2d_1_layer, AI_STATIC ,
  .in = &conv2d_1_output, .out = &batch_normalization_1_output, 
  .scale = &batch_normalization_1_scale, 
  .bias = &batch_normalization_1_bias, 
);
/* Layer #3: "max_pooling2d_1" (Pool) */
  

AI_LAYER_OBJ_DECLARE(
  max_pooling2d_1_layer, 3,
  POOL_TYPE,
  pool, forward_mp,
  &AI_NET_OBJ_INSTANCE, &conv2d_2_layer, AI_STATIC ,
  .in = &batch_normalization_1_output, .out = &max_pooling2d_1_output, 
  .pool_size = AI_SHAPE_2D_INIT(2, 1), 
  .pool_stride = AI_SHAPE_2D_INIT(2, 1), 
  .pool_pad = AI_SHAPE_2D_INIT(0, 0), 
);
/* Layer #4: "conv2d_2" (Conv2D) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  conv2d_2_weights_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 1600,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  conv2d_2_weights,
  AI_SHAPE_INIT(10, 4, 4, 10),
  AI_STRIDE_INIT(640, 160, 40, 4),
  &conv2d_2_weights_array,
  &conv2d_2_weights,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  conv2d_2_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  conv2d_2_bias,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &conv2d_2_bias_array,
  &conv2d_2_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  conv2d_2_layer, 4,
  CONV2D_TYPE,
  conv2d, forward_conv2d,
  &AI_NET_OBJ_INSTANCE, &batch_normalization_2_layer, AI_STATIC ,
  .in = &max_pooling2d_1_output, .out = &conv2d_2_output, 
  .weights = &conv2d_2_weights, 
  .bias = &conv2d_2_bias, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_2D_INIT(0, 0), 
);
/* Layer #5: "batch_normalization_2" (ScaleBias) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_2_scale_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  batch_normalization_2_scale,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_2_scale_array,
  &batch_normalization_2_scale,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_2_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  batch_normalization_2_bias,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_2_bias_array,
  &batch_normalization_2_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  batch_normalization_2_layer, 6,
  BN_TYPE,
  bn, forward_bn,
  &AI_NET_OBJ_INSTANCE, &max_pooling2d_2_layer, AI_STATIC ,
  .in = &conv2d_2_output, .out = &batch_normalization_2_output, 
  .scale = &batch_normalization_2_scale, 
  .bias = &batch_normalization_2_bias, 
);
/* Layer #6: "max_pooling2d_2" (Pool) */
  

AI_LAYER_OBJ_DECLARE(
  max_pooling2d_2_layer, 7,
  POOL_TYPE,
  pool, forward_mp,
  &AI_NET_OBJ_INSTANCE, &lstm_1_layer, AI_STATIC ,
  .in = &batch_normalization_2_output, .out = &max_pooling2d_2_output_in, 
  .pool_size = AI_SHAPE_2D_INIT(2, 2), 
  .pool_stride = AI_SHAPE_2D_INIT(2, 2), 
  .pool_pad = AI_SHAPE_2D_INIT(0, 0), 
);
/* Layer #7: "lstm_1" (LSTM) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  lstm_1_kernel_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 240,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  lstm_1_kernel,
  AI_SHAPE_INIT(40, 1, 1, 6),
  AI_STRIDE_INIT(24, 24, 24, 4),
  &lstm_1_kernel_array,
  &lstm_1_kernel,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  lstm_1_recurrent_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 400,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  lstm_1_recurrent,
  AI_SHAPE_INIT(40, 1, 1, 10),
  AI_STRIDE_INIT(40, 40, 40, 4),
  &lstm_1_recurrent_array,
  &lstm_1_recurrent,
  AI_STATIC)

/* Weight tensor #3 */
AI_ARRAY_OBJ_DECLARE(
  lstm_1_peephole_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 30,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  lstm_1_peephole,
  AI_SHAPE_INIT(1, 1, 30, 1),
  AI_STRIDE_INIT(120, 120, 4, 4),
  &lstm_1_peephole_array,
  &lstm_1_peephole,
  AI_STATIC)

/* Weight tensor #4 */
AI_ARRAY_OBJ_DECLARE(
  lstm_1_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 40,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  lstm_1_bias,
  AI_SHAPE_INIT(1, 1, 40, 1),
  AI_STRIDE_INIT(160, 160, 4, 4),
  &lstm_1_bias_array,
  &lstm_1_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  lstm_1_layer, 10,
  LSTM_TYPE,
  lstm, forward_lstm,
  &AI_NET_OBJ_INSTANCE, &batch_normalization_3_layer, AI_STATIC ,
  .in = &max_pooling2d_2_output_out, .out = &lstm_1_output, 
  .kernel = &lstm_1_kernel, 
  .recurrent = &lstm_1_recurrent, 
  .peephole = &lstm_1_peephole, 
  .bias = &lstm_1_bias, 
  .n_units = 10, 
  .activation_nl = ai_math_tanh, 
  .recurrent_nl = ai_math_hard_sigmoid, 
  .out_nl = ai_math_tanh, 
);
/* Layer #8: "batch_normalization_3" (ScaleBias) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_3_scale_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  batch_normalization_3_scale,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_3_scale_array,
  &batch_normalization_3_scale,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  batch_normalization_3_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 10,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  batch_normalization_3_bias,
  AI_SHAPE_INIT(1, 1, 10, 1),
  AI_STRIDE_INIT(40, 40, 4, 4),
  &batch_normalization_3_bias_array,
  &batch_normalization_3_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  batch_normalization_3_layer, 11,
  BN_TYPE,
  bn, forward_bn,
  &AI_NET_OBJ_INSTANCE, &activation_3_layer, AI_STATIC ,
  .in = &lstm_1_output, .out = &batch_normalization_3_output, 
  .scale = &batch_normalization_3_scale, 
  .bias = &batch_normalization_3_bias, 
);
/* Layer #9: "activation_3" (Nonlinearity) */
  

AI_LAYER_OBJ_DECLARE(
  activation_3_layer, 12,
  NL_TYPE,
  nl, forward_tanh,
  &AI_NET_OBJ_INSTANCE, &dense_1_layer, AI_STATIC ,
  .in = &batch_normalization_3_output, .out = &activation_3_output_in, 
);
/* Layer #10: "dense_1" (Dense) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  dense_1_weights_array, AI_DATA_FORMAT_LUT8_FLOAT, 
  NULL, NULL, 1500,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  dense_1_weights,
  AI_SHAPE_INIT(1, 1, 15, 100),
  AI_STRIDE_INIT(1500, 1500, 100, 1),
  &dense_1_weights_array,
  &dense_1_weights,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  dense_1_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 15,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  dense_1_bias,
  AI_SHAPE_INIT(1, 1, 15, 1),
  AI_STRIDE_INIT(60, 60, 4, 4),
  &dense_1_bias_array,
  &dense_1_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  dense_1_layer, 15,
  DENSE_TYPE,
  dense, forward_dense,
  &AI_NET_OBJ_INSTANCE, &activation_4_layer, AI_STATIC ,
  .in = &activation_3_output_out, .out = &dense_1_output, 
  .weights = &dense_1_weights, 
  .bias = &dense_1_bias, 
);
/* Layer #11: "activation_4" (Nonlinearity) */
  

AI_LAYER_OBJ_DECLARE(
  activation_4_layer, 16,
  NL_TYPE,
  nl, forward_relu,
  &AI_NET_OBJ_INSTANCE, &dense_2_layer, AI_STATIC ,
  .in = &dense_1_output, .out = &activation_4_output, 
);
/* Layer #12: "dense_2" (Dense) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  dense_2_weights_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 225,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  dense_2_weights,
  AI_SHAPE_INIT(1, 1, 15, 15),
  AI_STRIDE_INIT(900, 900, 60, 4),
  &dense_2_weights_array,
  &dense_2_weights,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  dense_2_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 15,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  dense_2_bias,
  AI_SHAPE_INIT(1, 1, 15, 1),
  AI_STRIDE_INIT(60, 60, 4, 4),
  &dense_2_bias_array,
  &dense_2_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  dense_2_layer, 17,
  DENSE_TYPE,
  dense, forward_dense,
  &AI_NET_OBJ_INSTANCE, &activation_5_layer, AI_STATIC ,
  .in = &activation_4_output, .out = &dense_2_output, 
  .weights = &dense_2_weights, 
  .bias = &dense_2_bias, 
);
/* Layer #13: "activation_5" (Nonlinearity) */
  

AI_LAYER_OBJ_DECLARE(
  activation_5_layer, 18,
  NL_TYPE,
  nl, forward_relu,
  &AI_NET_OBJ_INSTANCE, &dense_3_layer, AI_STATIC ,
  .in = &dense_2_output, .out = &activation_5_output, 
);
/* Layer #14: "dense_3" (Dense) */
  

/* Weight tensor #1 */
AI_ARRAY_OBJ_DECLARE(
  dense_3_weights_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 45,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  dense_3_weights,
  AI_SHAPE_INIT(1, 1, 3, 15),
  AI_STRIDE_INIT(180, 180, 60, 4),
  &dense_3_weights_array,
  &dense_3_weights,
  AI_STATIC)

/* Weight tensor #2 */
AI_ARRAY_OBJ_DECLARE(
  dense_3_bias_array, AI_DATA_FORMAT_FLOAT, 
  NULL, NULL, 3,
  AI_STATIC )

AI_TENSOR_OBJ_DECLARE(
  dense_3_bias,
  AI_SHAPE_INIT(1, 1, 3, 1),
  AI_STRIDE_INIT(12, 12, 4, 4),
  &dense_3_bias_array,
  &dense_3_bias,
  AI_STATIC)

AI_LAYER_OBJ_DECLARE(
  dense_3_layer, 19,
  DENSE_TYPE,
  dense, forward_dense,
  &AI_NET_OBJ_INSTANCE, &activation_6_layer, AI_STATIC ,
  .in = &activation_5_output, .out = &dense_3_output, 
  .weights = &dense_3_weights, 
  .bias = &dense_3_bias, 
);
/* Layer #15: "activation_6" (Nonlinearity) */
  

AI_LAYER_OBJ_DECLARE(
  activation_6_layer, 20,
  SM_TYPE,
  sm, forward_sm,
  &AI_NET_OBJ_INSTANCE, &activation_6_layer, AI_STATIC ,
  .in = &dense_3_output, .out = &activation_6_output, 
);


AI_NETWORK_OBJ_DECLARE(
  AI_NET_OBJ_INSTANCE,
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 13896, 1,
                     NULL),
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 8644, 1,
                     NULL),
  &input_0_output, &activation_6_output,
  &conv2d_1_layer, 0)


AI_DECLARE_STATIC
ai_bool network_configure_activations(
  ai_network* net_ctx, const ai_buffer* activation_buffer)
{
  AI_ASSERT(net_ctx &&  activation_buffer && activation_buffer->data)

  ai_ptr activations = AI_PTR(AI_PTR_ALIGN(activation_buffer->data, 4));
  AI_ASSERT( activations )
  AI_FLAG_SET(net_ctx->flags, AI_NETWORK_FLAG_OUT_COPY);

  {
    /* Updating activations (byte) offsets */
    input_0_output_array.data = NULL;
  input_0_output_array.data_start = NULL;
  conv2d_1_output_array.data = activations + 0;
  conv2d_1_output_array.data_start = activations + 0;
  batch_normalization_1_output_array.data = activations + 0;
  batch_normalization_1_output_array.data_start = activations + 0;
  max_pooling2d_1_output_array.data = activations + 5760;
  max_pooling2d_1_output_array.data_start = activations + 5760;
  conv2d_2_output_array.data = activations + 0;
  conv2d_2_output_array.data_start = activations + 0;
  batch_normalization_2_output_array.data = activations + 0;
  batch_normalization_2_output_array.data_start = activations + 0;
  max_pooling2d_2_output_in_array.data = activations + 1200;
  max_pooling2d_2_output_in_array.data_start = activations + 1200;
  max_pooling2d_2_output_out_array.data = activations + 1200;
  max_pooling2d_2_output_out_array.data_start = activations + 1200;
  lstm_1_output_array.data = activations + 0;
  lstm_1_output_array.data_start = activations + 0;
  batch_normalization_3_output_array.data = activations + 0;
  batch_normalization_3_output_array.data_start = activations + 0;
  activation_3_output_in_array.data = activations + 0;
  activation_3_output_in_array.data_start = activations + 0;
  activation_3_output_out_array.data = activations + 0;
  activation_3_output_out_array.data_start = activations + 0;
  dense_1_output_array.data = activations + 400;
  dense_1_output_array.data_start = activations + 400;
  activation_4_output_array.data = activations + 400;
  activation_4_output_array.data_start = activations + 400;
  dense_2_output_array.data = activations + 0;
  dense_2_output_array.data_start = activations + 0;
  activation_5_output_array.data = activations + 0;
  activation_5_output_array.data_start = activations + 0;
  dense_3_output_array.data = activations + 60;
  dense_3_output_array.data_start = activations + 60;
  activation_6_output_array.data = activations + 60;
  activation_6_output_array.data_start = activations + 60;
  
  }
  return true;
}

AI_DECLARE_STATIC
ai_bool network_configure_weights(
  ai_network* net_ctx, const ai_buffer* weights_buffer)
{
  AI_ASSERT(net_ctx &&  weights_buffer && weights_buffer->data)

  ai_ptr weights = AI_PTR(weights_buffer->data);
  AI_ASSERT( weights )

  {
    /* Updating weights (byte) offsets */
    conv2d_1_weights_array.format |= AI_FMT_FLAG_CONST;
  conv2d_1_weights_array.data = weights + 0;
  conv2d_1_weights_array.data_start = weights + 0;
  conv2d_1_bias_array.format |= AI_FMT_FLAG_CONST;
  conv2d_1_bias_array.data = weights + 600;
  conv2d_1_bias_array.data_start = weights + 600;
  batch_normalization_1_scale_array.format |= AI_FMT_FLAG_CONST;
  batch_normalization_1_scale_array.data = weights + 640;
  batch_normalization_1_scale_array.data_start = weights + 640;
  batch_normalization_1_bias_array.format |= AI_FMT_FLAG_CONST;
  batch_normalization_1_bias_array.data = weights + 680;
  batch_normalization_1_bias_array.data_start = weights + 680;
  conv2d_2_weights_array.format |= AI_FMT_FLAG_CONST;
  conv2d_2_weights_array.data = weights + 720;
  conv2d_2_weights_array.data_start = weights + 720;
  conv2d_2_bias_array.format |= AI_FMT_FLAG_CONST;
  conv2d_2_bias_array.data = weights + 7120;
  conv2d_2_bias_array.data_start = weights + 7120;
  batch_normalization_2_scale_array.format |= AI_FMT_FLAG_CONST;
  batch_normalization_2_scale_array.data = weights + 7160;
  batch_normalization_2_scale_array.data_start = weights + 7160;
  batch_normalization_2_bias_array.format |= AI_FMT_FLAG_CONST;
  batch_normalization_2_bias_array.data = weights + 7200;
  batch_normalization_2_bias_array.data_start = weights + 7200;
  lstm_1_kernel_array.format |= AI_FMT_FLAG_CONST;
  lstm_1_kernel_array.data = weights + 7240;
  lstm_1_kernel_array.data_start = weights + 7240;
  lstm_1_recurrent_array.format |= AI_FMT_FLAG_CONST;
  lstm_1_recurrent_array.data = weights + 8200;
  lstm_1_recurrent_array.data_start = weights + 8200;
  lstm_1_peephole_array.format |= AI_FMT_FLAG_CONST;
  lstm_1_peephole_array.data = weights + 9800;
  lstm_1_peephole_array.data_start = weights + 9800;
  lstm_1_bias_array.format |= AI_FMT_FLAG_CONST;
  lstm_1_bias_array.data = weights + 9920;
  lstm_1_bias_array.data_start = weights + 9920;
  batch_normalization_3_scale_array.format |= AI_FMT_FLAG_CONST;
  batch_normalization_3_scale_array.data = weights + 10080;
  batch_normalization_3_scale_array.data_start = weights + 10080;
  batch_normalization_3_bias_array.format |= AI_FMT_FLAG_CONST;
  batch_normalization_3_bias_array.data = weights + 10120;
  batch_normalization_3_bias_array.data_start = weights + 10120;
  dense_1_weights_array.format |= AI_FMT_FLAG_CONST;
  dense_1_weights_array.data = weights + 10160;
  dense_1_weights_array.data_start = weights + 11660;
  dense_1_bias_array.format |= AI_FMT_FLAG_CONST;
  dense_1_bias_array.data = weights + 12684;
  dense_1_bias_array.data_start = weights + 12684;
  dense_2_weights_array.format |= AI_FMT_FLAG_CONST;
  dense_2_weights_array.data = weights + 12744;
  dense_2_weights_array.data_start = weights + 12744;
  dense_2_bias_array.format |= AI_FMT_FLAG_CONST;
  dense_2_bias_array.data = weights + 13644;
  dense_2_bias_array.data_start = weights + 13644;
  dense_3_weights_array.format |= AI_FMT_FLAG_CONST;
  dense_3_weights_array.data = weights + 13704;
  dense_3_weights_array.data_start = weights + 13704;
  dense_3_bias_array.format |= AI_FMT_FLAG_CONST;
  dense_3_bias_array.data = weights + 13884;
  dense_3_bias_array.data_start = weights + 13884;
  
  }

  return true;
}

/*** PUBLIC APIs SECTION  *****************************************************/

AI_API_ENTRY
ai_bool ai_network_get_info(
  ai_handle network, ai_network_report* report)
{
  ai_network* net_ctx = AI_NETWORK_ACQUIRE_CTX(network);

  if ( report && net_ctx )
  {
    ai_network_report r = {
      .model_name        = AI_NETWORK_MODEL_NAME,
      .model_signature   = AI_NETWORK_MODEL_SIGNATURE,
      .model_datetime    = AI_TOOLS_DATE_TIME,
      
      .compile_datetime  = AI_TOOLS_COMPILE_TIME,
      
      .runtime_revision  = ai_platform_runtime_get_revision(),
      .runtime_version   = ai_platform_runtime_get_version(),

      .tool_revision     = AI_TOOLS_REVISION_ID,
      .tool_version      = {AI_TOOLS_VERSION_MAJOR, AI_TOOLS_VERSION_MINOR,
                            AI_TOOLS_VERSION_MICRO, 0x0},
      .tool_api_version  = {AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR,
                            AI_TOOLS_API_VERSION_MICRO, 0x0},

      .api_version            = ai_platform_api_get_version(),
      .interface_api_version  = ai_platform_interface_api_get_version(),
      
      .n_macc            = 86465,
      .n_inputs          = AI_NETWORK_IN_NUM,
      .inputs            = AI_BUFFER_OBJ_INIT(
                              AI_BUFFER_FORMAT_FLOAT,
                              20,
                              11,
                              1,
                              1, NULL),
      .n_outputs         = AI_NETWORK_OUT_NUM,
      .outputs           = AI_BUFFER_OBJ_INIT(
                              AI_BUFFER_FORMAT_FLOAT,
                              1,
                              1,
                              3,
                              1, NULL),
      .activations       = net_ctx->activations,
      .weights           = net_ctx->params,
      .n_nodes           = 0,
      .signature         = net_ctx->signature,
    };

    AI_FOR_EACH_NODE_DO(node, net_ctx->input_node)
    {
      r.n_nodes++;
    }

    *report = r;

    return ( r.n_nodes>0 ) ? true : false;
  }
  
  return false;
}

AI_API_ENTRY
ai_error ai_network_get_error(ai_handle network)
{
  return ai_platform_network_get_error(network);
}

AI_API_ENTRY
ai_error ai_network_create(
  ai_handle* network, const ai_buffer* network_config)
{
  return ai_platform_network_create(
    network, network_config, 
    &AI_NET_OBJ_INSTANCE,
    AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR, AI_TOOLS_API_VERSION_MICRO);
}

AI_API_ENTRY
ai_handle ai_network_destroy(ai_handle network)
{
  return ai_platform_network_destroy(network);
}

AI_API_ENTRY
ai_bool ai_network_init(
  ai_handle network, const ai_network_params* params)
{
  ai_network* net_ctx = ai_platform_network_init(network, params);
  if ( !net_ctx ) return false;

  ai_bool ok = network_configure_weights(net_ctx, &params->params);

  ok &= network_configure_activations(net_ctx, &params->activations);
  
  return ok;
}


AI_API_ENTRY
ai_i32 ai_network_run(
  ai_handle network, const ai_buffer* input, ai_buffer* output)
{
  return ai_platform_network_process(network, input, output);
}

AI_API_ENTRY
ai_i32 ai_network_forward(ai_handle network, const ai_buffer* input)
{
  return ai_platform_network_process(network, input, NULL);
}

#undef AI_NETWORK_MODEL_SIGNATURE
#undef AI_NET_OBJ_INSTANCE
#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#undef AI_TOOLS_DATE_TIME
#undef AI_TOOLS_COMPILE_TIME

