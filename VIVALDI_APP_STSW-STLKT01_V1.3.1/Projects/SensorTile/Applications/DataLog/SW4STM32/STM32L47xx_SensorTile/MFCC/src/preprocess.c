/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

#define ARM_MATH_CM4

#include <arm_math.h>
#include <stdbool.h>
#include "wave.h"

#include "melFilOpt.h"
#include "fft_window.h"
#include "dctCos.h"

/* USER CODE BEGIN Includes */

//#define SAMPLES 11025 // 1s
#define SAMPLES 5512 // 0.5s

/* stft params */
#define FFT_SIZE 2048
#define HOP_LENGHT 512

/* reflect padding params */
#define PAD_WIDTH FFT_SIZE/2
#define ARRAY_LEN FFT_SIZE		// input array
#define PADDED_LENGHT SAMPLES+(FFT_SIZE)

/* framing func params */
#define FRAME_LENGHT FFT_SIZE

/* mel params */
#define FILTER_LEN  2022

static float32_t out[FFT_SIZE] __attribute__ ((section (".ram2")));	    // fft function output
static float32_t frame[FRAME_LENGHT] __attribute__ ((section (".ram2")));	// frame from frames matrix
static float32_t s_filtered[ROWS] __attribute__ ((section (".ram2")));

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

static arm_rfft_fast_instance_f32 rfft;

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

void reflect_padding( int16_t *output_array)
{
    int i, j, x;

    /* flag to invert the array */
    bool inverse = false;

    /* left padding */
    x = 1;  /* input_array index */
    j = PAD_WIDTH - 1;    /* output_array index */
    for (i = 0; i < PAD_WIDTH; i++)
    {
        if (x > ARRAY_LEN - 1 && !inverse)
        {
            inverse = true;
            x = ARRAY_LEN - 2;
        }
        else if (x == -1 && inverse)
        {
            inverse = false;
            x = 1;
        }

        output_array[j] = output_array[x + PAD_WIDTH]; /* modification of left wing of audio */

        if(!inverse)
            x++;
        else
            x--;
        j--;
    }

    inverse = true;

    /* right padding */
    x = SAMPLES - 2;  /* input_array index */
    j = PADDED_LENGHT - PAD_WIDTH ;    /* output_array index   j = PAD_WIDTH */
    for (i = 0; i < PAD_WIDTH; i++)
    {
        if ( ( x > ARRAY_LEN - 1 ) && (!inverse) )
        {
            inverse = true;
            x = ARRAY_LEN - 2;
        }
        else if ( (x == -1 )  && ( inverse ) )
        {
            inverse = false;
            x = 1;
        }

        output_array[j] = output_array[x - PAD_WIDTH]; /* modification of audio's right wing */

        if(!inverse)
            x++;
        else
            x--;

        j++;
    }
}

void apply_hann(float *in)
{
	for (int i = 0; i < FFT_SIZE; i++)
	{
	  in[i] = in[i] * fft_window[i];
	}
}

void dct(float in_buf[ROWS], int mfcc_col, float mfcc[COEFFS][COLS][1])
{

    int j, l;
    j = 0;
    l = 0;

    /* dct will store the discrete cosine transform */

    float cj, dct1, sum;

    for (j = 0; j < COEFFS; j++)
    {
    	/* ci and cj depends on frequency as well as number of row and columns of specified matrix */

        if (j == 0)
        {
        	cj = CI;
        }
        else
        {
        	cj = CJ;
        }

        /* sum will temporarily store the sum of cosine signals */
        sum = 0;
        for (l = 0; l < ROWS; l++)
        {
        	dct1 = 2 * in_buf[l] * dct_cos[j][l];
        	sum = sum + dct1;
        }
        mfcc[j][mfcc_col][0] = cj * sum;
     }
}

void AudioPreProcess_mfcc(int16_t * audio_out_buffer, float mfcc[COEFFS][COLS][1])
{
	arm_rfft_fast_init_f32(&rfft, FFT_SIZE);

	/* reflect padding of input wave */
	reflect_padding( audio_out_buffer );

	for (int i = 0; i < COLS; i++)
	{
		for (int j = 0; j < FRAME_LENGHT; j++)
		{
			int16_t zVal = audio_out_buffer[ ( HOP_LENGHT * i ) + j];
			if ( zVal >= 0 )
			{
				frame[j] = ( zVal / 65536.0 ) -1;

			}
			else
			{
				frame[j] = ( zVal / 65536.0 ) + 1;
			}
		}

		apply_hann(frame);

		/* compute fft of single frame */
		arm_rfft_fast_f32(&rfft, frame, out, 0);

		/* get values in usable form */
		float first_energy = out[0] * out[0];
		float last_energy =  out[1] * out[1];

		for (int a = 1; a < FFT_SIZE/2; a++)
		{
			float real = out[a*2];
			float im = out[a*2 + 1];
			frame[a] = real*real + im*im; /* reusing frame array instead of spectrum*/
		}

		/* final output spectrum */
		frame[0] = first_energy; /* reusing frame instead of spectrum */
		frame[FFT_SIZE/2] = last_energy;
		uint8_t zVal = (uint8_t)255;

		for (int filterIdx = 0; filterIdx < FILTER_LEN; filterIdx++)
		{
			if ( zVal != mel_rowIndex[filterIdx] )
			{
				s_filtered[mel_rowIndex[filterIdx]] = 0;
			}

			/* spectrum replaced with frame  itself */
			s_filtered[mel_rowIndex[filterIdx]] += frame[mel_colIndex[filterIdx]] * mel_flatten[filterIdx];
			zVal = mel_rowIndex[filterIdx];
		}

		for ( int pwdbIdx = 0; pwdbIdx < ROWS; pwdbIdx++)
		{
			s_filtered[pwdbIdx] = 10 * log10(s_filtered[pwdbIdx]);
		}

		dct(s_filtered, i, mfcc);

    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
