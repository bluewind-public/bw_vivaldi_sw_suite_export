#define COLS 11
#define ROWS 128
#define COEFFS 20
#define SQRT_2 1.414213562
#define CI  0.044194 // sqrt(128)
#define CJ  0.0625  // sqrt(2/128)

void AudioPreProcess_mfcc(int16_t * audio_wave, float mfcc[COEFFS][COLS][1]);
